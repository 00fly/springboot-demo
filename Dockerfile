#基础镜像
FROM adoptopenjdk/openjdk8-openj9:alpine-slim

COPY docker/wait-for.sh /
RUN chmod +x /wait-for.sh && \
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo 'Asia/Shanghai' >/etc/timezone

#安装DejaVu 字体库并将Alpine Linux 的软件源替换为阿里云镜像
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && \
    apk add --update ttf-dejavu fontconfig && \
    rm -rf /var/cache/apk/* && \
    mkfontscale && mkfontdir && fc-cache

#引入运行包
COPY target/*.jar /app.jar

#指定交互端口
EXPOSE 8081

CMD ["--server.port=8081"]

#项目的启动方式
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Xshareclasses", "-Xquickstart", "-jar", "/app.jar"]

package com.fly.demo.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

/**
 * LongAdder控制线程并发数
 *
 */
@Slf4j
public class LongAdderTest
{
    private LongAdder count = new LongAdder();
    
    /**
     * 最大并发数
     */
    private int max = 5;
    
    /**
     * 线程池方式测试
     * 
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        int id = 0;
        while (id++ < 100)
        {
            cachedThreadPool.execute(() -> runCall());
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(100, 1000));
        }
        cachedThreadPool.shutdown();
        while (!cachedThreadPool.isTerminated()) // 保证任务全部执行完
        {
        }
    }
    
    private void runCall()
    {
        try
        {
            count.increment();
            log.info("++++ 计数器自增：{}", count.sum());
            if (count.sum() > max)
            {
                log.info("✈✈✈✈✈ 请求用户过多,请稍后再试! ✈✈✈✈✈");
                return;
            }
            
            // 模拟耗时业务操作
            log.info("★★★★★★★★ 报名或抢购处理中★★★★★★★★");
            StopWatch clock = new StopWatch();
            clock.start();
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(1000, 5000));
            clock.stop();
            log.info("运行 {} ms ---------------", clock.getLastTaskTimeMillis());
        }
        catch (InterruptedException e)
        {
            log.error(e.getMessage());
        }
        finally
        {
            count.decrement();
            log.info("---- 计数器自减：{}", count.sum());
        }
    }
}
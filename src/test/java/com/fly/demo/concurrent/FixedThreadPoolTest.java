package com.fly.demo.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

/**
 * 固定长度线程池控制并发
 */
@Slf4j
public class FixedThreadPoolTest
{
    private int max = 5;
    
    private AtomicInteger count = new AtomicInteger(0);
    
    /**
     * 线程池方式测试
     * 
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        ExecutorService executorService = Executors.newFixedThreadPool(max);
        int id = 0;
        while (id++ < 100)
        {
            executorService.execute(() -> runCall());
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(100, 1000));
        }
        executorService.shutdown();
        while (!executorService.isTerminated()) // 保证任务全部执行完
        {
        }
    }
    
    /**
     * 验证方法: sum('报名或抢购处理中')与sum('请求用户过多,请稍后再试')日志条数为100
     */
    private void runCall()
    {
        try
        {
            log.info("计数器自增：{}", count.incrementAndGet());
            
            // 模拟耗时业务操作
            log.info("★★★★★★★★ 报名或抢购处理中★★★★★★★★");
            StopWatch clock = new StopWatch();
            clock.start();
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(1000, 5000));
            clock.stop();
            log.info("运行 {} ms ---------------", clock.getLastTaskTimeMillis());
        }
        catch (InterruptedException e)
        {
            log.error(e.getMessage());
        }
        finally
        {
            log.info("计数器自减：{}", count.decrementAndGet());
        }
    }
}

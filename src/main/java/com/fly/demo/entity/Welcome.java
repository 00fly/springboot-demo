package com.fly.demo.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 
 * Welcome配置文件实体<br>
 * 使用@Lazy待SysDataBaseConfig方法initDatabasePropertySource执行完再注入<br>
 * 否则仅使用数据库初始化时开发环境和Jar运行message值不一致
 * 
 * @author 00fly
 * @version [版本号, 2023年11月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
@Lazy
@Component
@ConfigurationProperties(prefix = "welcome")
public class Welcome
{
    /**
     * message赋值方式：<br>
     * 1. Configuration注解在SysDataBaseConfig<br>
     * 2. spring.profiles.active指定dev即application-dev.yml<br>
     * 3. welcome.properties内容变更时触发<br>
     * 4. /show/refresh接口被调用时触发<br>
     * 方式1、2有竞争，不能严格区分先后
     */
    private String message = "hello, 00fly in java!";
}

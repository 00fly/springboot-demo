package com.fly.demo.tcp.base;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 初始化管理类
 */
@Service
public class InitManager
{
    TcpServer server = new TcpServer();
    
    TcpClient client = new TcpClient("CLIENT_1");
    
    public TcpServer getServer()
    {
        return server;
    }
    
    public TcpClient getClient()
    {
        return client;
    }
    
    /**
     * 启动TcpServer、TcpClient
     */
    @PostConstruct
    private void init()
    {
        if (server.startServer("0.0.0.0", 8000))
        {
            new Thread(server).start();
        }
        // docker环境下优先使用docker-compose中environment值
        String serverIp = StringUtils.defaultIfBlank(System.getenv().get("TCP_SERVER"), "127.0.0.1");
        if (client.connectServer(serverIp, 8000))
        {
            new Thread(client).start();
        }
    }
}

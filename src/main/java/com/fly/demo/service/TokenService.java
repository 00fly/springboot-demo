package com.fly.demo.service;

public interface TokenService
{
    /**
     * 验证token是否合法
     * 
     * @param token
     * @return
     */
    public boolean valide(String token);
}

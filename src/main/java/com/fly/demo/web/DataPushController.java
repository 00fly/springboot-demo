package com.fly.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.fly.demo.sse.SSEServer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Api(tags = "SSE")
@RequestMapping("/sse")
public class DataPushController
{
    @CrossOrigin
    @GetMapping("/connect/{userId}")
    public SseEmitter connect(@PathVariable String userId)
    {
        return SSEServer.connect();
    }
    
    @ApiOperation("触发推送")
    @ResponseBody
    @PostMapping("/touch")
    public void touch(String content)
    {
        SSEServer.batchSendMessage(content);
    }
}

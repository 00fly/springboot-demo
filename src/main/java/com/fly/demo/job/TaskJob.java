package com.fly.demo.job;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import com.fly.demo.entity.Welcome;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class TaskJob
{
    @Autowired
    private Welcome welcome;
    
    /**
     * 不能实时刷新
     */
    @Value("${welcome.message:hello, 00fly in java!}")
    String msg;
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    /**
     * 测试日志打印
     */
    @Scheduled(fixedRate = 60 * 1000L)
    public void run()
    {
        log.trace("★★★★★★★★ {}, {}", welcome.getMessage(), msg);
        log.debug("★★★★★★★★ {}, {}", welcome.getMessage(), msg);
        log.info("★★★★★★★★ {}, {}", welcome.getMessage(), msg);
        log.warn("★★★★★★★★ {}, {}", welcome.getMessage(), msg);
    }
    
    /**
     * 批量操作
     */
    @Scheduled(fixedRate = 10000)
    public void batch()
    {
        String sql = "INSERT INTO `user` (`username`, `password`, `create_time`) VALUES (?, ?, NOW())";
        List<Object[]> batchArgs = new ArrayList<Object[]>();
        IntStream.rangeClosed(1, 100).forEach(i -> batchArgs.add(new Object[] {RandomStringUtils.randomAlphabetic(10), RandomStringUtils.randomAlphanumeric(10)}));
        jdbcTemplate.batchUpdate(sql, batchArgs);
    }
}

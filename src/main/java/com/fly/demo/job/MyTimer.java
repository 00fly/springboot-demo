package com.fly.demo.job;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fly.demo.entity.Welcome;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MyTimer
{
    @Autowired
    private Welcome welcome;
    
    /**
     * Timer线程安全, 但只会单线程执行, 如果执行时间过长, 就错过下次任务了, 抛出异常时, task会终止
     */
    @PostConstruct
    public void init()
    {
        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                log.info(welcome.getMessage());
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 10000L, 60 * 1000L);
        log.info("======== Timer started!");
    }
}

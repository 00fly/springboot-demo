package com.fly;

import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableCaching
@EnableScheduling
@SpringBootApplication
public class SimpleApplication
{
    public static void main(String[] args)
    {
        Stream.of(args).forEach(log::info);
        SpringApplication.run(SimpleApplication.class, args);
    }
}

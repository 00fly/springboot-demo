package com.fly.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ResourceUtils;

@Configuration
@Profile("dev")
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "jdbc-h2.properties")
class H2Config
{
}

@Configuration
@Profile({"test", "prod"})
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "jdbc-mysql.properties")
class MysqlConfig
{
}
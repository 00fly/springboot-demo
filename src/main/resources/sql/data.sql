INSERT INTO `user` VALUES ('1', 'username_001', '111111', NOW(), NOW());
INSERT INTO `user` VALUES ('2', 'username_002', '222222', NOW(), NOW());

--重复插入，模拟异常
INSERT INTO `user` VALUES ('1', 'username_001', '123456', NOW(), NOW());

INSERT INTO `user` VALUES ('3', 'username_003', '333333', NOW(), NOW());
INSERT INTO `user` VALUES ('4', 'username_004', '444444', NOW(), NOW());


INSERT INTO `sys_config` VALUES ('1', 'welcome.message',  CONCAT('hello from db at ' , now()), '系统提示语', '1', '0', 'admin', now(), 'admin', now());